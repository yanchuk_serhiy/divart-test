var STORAGE_KEY = 'vue-js-todo-P7oZi9sL';
var todoStorage = {
    fetch: function () {
        return JSON.parse(localStorage.getItem(STORAGE_KEY) || 'null');
    },
    save: function (global) {
        localStorage.setItem(STORAGE_KEY, JSON.stringify(global));
    }
};
var GLOBAL;

if (todoStorage.fetch())
{
    GLOBAL = todoStorage.fetch();
}
else
{
    GLOBAL = {
        todos: [],
        tags: [],
        addForm: {
            name: '',
            status: '',
            date: '',
            tag: ''
        },

        filter: null,
        activeFilter: null,
        tagFilter: null
    };

    todoStorage.save(GLOBAL);
}


new Vue ({
    el: '#addTask',
    data: GLOBAL,
    watch: {
        todos: function(todos) {
            todoStorage.save(GLOBAL);
        }
    },
    methods: {
        saveTask: function () {
            var _this = this;
            GLOBAL.todos.push({
                name: GLOBAL.addForm.name,
                status: GLOBAL.addForm.status,
                date: GLOBAL.addForm.date,
                tag: GLOBAL.addForm.tag,
                activity: true
            });

            GLOBAL.tags.push({
                tagName: GLOBAL.addForm.tag
            });


            $('#addTask').modal('hide');
            GLOBAL.addForm.name = '';
            GLOBAL.addForm.status = '';
            GLOBAL.addForm.date = '';
            GLOBAL.addForm.tag = '';

            GLOBAL.filter = null;
            GLOBAL.activeFilter = null;
            GLOBAL.tagFilter = null;

            audio.play();

        }
    }
});



var ShowTasks = new Vue ({
   el: '#tasksList',
    data: GLOBAL,
    methods: {
        deleteTodo: function(index) {
            GLOBAL.todos.splice(index, 1);
            audio.play();
        },
        updateTasks: function () {
            GLOBAL.todos = todoStorage.fetch();
        },
        addToDone: function (index) {
            GLOBAL.todos[index].activity = true;
            todoStorage.save(GLOBAL);

        },
        addToActive: function (index) {
            GLOBAL.todos[index].activity = false;
            todoStorage.save(GLOBAL);

        }
    }

});

var filters = new Vue ({
    el: '#statuses',
    data: GLOBAL,
    methods : {
        SetFilter: function (status) {
            GLOBAL.filter = status;
            GLOBAL.tagFilter = null;
            GLOBAL.activeFilter = null;
        },
        
        SetNull:function () {
            GLOBAL.filter = null;
            GLOBAL.tagFilter = null;
            GLOBAL.activeFilter = null;
        }
    }
});

var tags = new Vue({
    el: '#tags',
    data: GLOBAL,
    watch:{
        todos: function(todos) {
            todoStorage.fetch();
        }
    },
    methods: {
        setTag: function (val) {
            GLOBAL.tagFilter = val;
            GLOBAL.filter = null;
            GLOBAL.activeFilter = null;
        },
        tagNull: function () {
            GLOBAL.tagFilter = null;
            GLOBAL.filter = null;
            GLOBAL.activeFilter = null;
        }
    }
});


new Vue({
    el: '#filtersActivities',
    data: GLOBAL,
    watch: {
        todos: function(todos) {
            todoStorage.fetch();
        }
    },
    methods: {
        setActive: function (val) {
            GLOBAL.activeFilter = val;
            GLOBAL.filter = null;
            GLOBAL.tagFilter = null;
        },
        activeNull: function () {
            GLOBAL.activeFilter = null;
            GLOBAL.filter = null;
            GLOBAL.tagFilter = null;
        }
    }
});


var audio = new Vue({
    el: '#audio',
    methods: {
        play: function(event) {
            this.$refs.audioElm.play();
        }
    }
});

