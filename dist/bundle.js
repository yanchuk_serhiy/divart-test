/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__scss_base_scss__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__scss_base_scss___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__scss_base_scss__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__scss_modal_scss__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__scss_modal_scss___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__scss_modal_scss__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__scss_header_scss__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__scss_header_scss___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__scss_header_scss__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__scss_nav_scss__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__scss_nav_scss___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__scss_nav_scss__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__scss_status_scss__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__scss_status_scss___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__scss_status_scss__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__scss_flex_section_scss__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__scss_flex_section_scss___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5__scss_flex_section_scss__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__scss_scroll_scss__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__scss_scroll_scss___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6__scss_scroll_scss__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__scss_task_scss__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__scss_task_scss___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7__scss_task_scss__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__js_addTask_js__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__js_addTask_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8__js_addTask_js__);















/***/ }),
/* 1 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 2 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 3 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 4 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 5 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 6 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 7 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 8 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 9 */
/***/ (function(module, exports) {

var STORAGE_KEY = 'vue-js-todo-P7oZi9sL';
var todoStorage = {
    fetch: function () {
        return JSON.parse(localStorage.getItem(STORAGE_KEY) || 'null');
    },
    save: function (global) {
        localStorage.setItem(STORAGE_KEY, JSON.stringify(global));
    }
};
var GLOBAL;

if (todoStorage.fetch())
{
    GLOBAL = todoStorage.fetch();
}
else
{
    GLOBAL = {
        todos: [],
        tags: [],
        addForm: {
            name: '',
            status: '',
            date: '',
            tag: ''
        },

        filter: null,
        activeFilter: null,
        tagFilter: null
    };

    todoStorage.save(GLOBAL);
}


new Vue ({
    el: '#addTask',
    data: GLOBAL,
    watch: {
        todos: function(todos) {
            todoStorage.save(GLOBAL);
        }
    },
    methods: {
        saveTask: function () {
            var _this = this;
            GLOBAL.todos.push({
                name: GLOBAL.addForm.name,
                status: GLOBAL.addForm.status,
                date: GLOBAL.addForm.date,
                tag: GLOBAL.addForm.tag,
                activity: true
            });

            GLOBAL.tags.push({
                tagName: GLOBAL.addForm.tag
            });


            $('#addTask').modal('hide');
            GLOBAL.addForm.name = '';
            GLOBAL.addForm.status = '';
            GLOBAL.addForm.date = '';
            GLOBAL.addForm.tag = '';

            GLOBAL.filter = null;
            GLOBAL.activeFilter = null;
            GLOBAL.tagFilter = null;

            audio.play();

        }
    }
});



var ShowTasks = new Vue ({
   el: '#tasksList',
    data: GLOBAL,
    methods: {
        deleteTodo: function(index) {
            GLOBAL.todos.splice(index, 1);
            audio.play();
        },
        updateTasks: function () {
            GLOBAL.todos = todoStorage.fetch();
        },
        addToDone: function (index) {
            GLOBAL.todos[index].activity = true;
            todoStorage.save(GLOBAL);

        },
        addToActive: function (index) {
            GLOBAL.todos[index].activity = false;
            todoStorage.save(GLOBAL);

        }
    }

});

var filters = new Vue ({
    el: '#statuses',
    data: GLOBAL,
    methods : {
        SetFilter: function (status) {
            GLOBAL.filter = status;
            GLOBAL.tagFilter = null;
            GLOBAL.activeFilter = null;
        },
        
        SetNull:function () {
            GLOBAL.filter = null;
            GLOBAL.tagFilter = null;
            GLOBAL.activeFilter = null;
        }
    }
});

var tags = new Vue({
    el: '#tags',
    data: GLOBAL,
    watch:{
        todos: function(todos) {
            todoStorage.fetch();
        }
    },
    methods: {
        setTag: function (val) {
            GLOBAL.tagFilter = val;
            GLOBAL.filter = null;
            GLOBAL.activeFilter = null;
        },
        tagNull: function () {
            GLOBAL.tagFilter = null;
            GLOBAL.filter = null;
            GLOBAL.activeFilter = null;
        }
    }
});


new Vue({
    el: '#filtersActivities',
    data: GLOBAL,
    watch: {
        todos: function(todos) {
            todoStorage.fetch();
        }
    },
    methods: {
        setActive: function (val) {
            GLOBAL.activeFilter = val;
            GLOBAL.filter = null;
            GLOBAL.tagFilter = null;
        },
        activeNull: function () {
            GLOBAL.activeFilter = null;
            GLOBAL.filter = null;
            GLOBAL.tagFilter = null;
        }
    }
});


var audio = new Vue({
    el: '#audio',
    methods: {
        play: function(event) {
            this.$refs.audioElm.play();
        }
    }
});



/***/ })
/******/ ]);